from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Angelina Condro' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2000,3,28) #TODO Implement this, format (Year, Month, Date)
npm = 1706043342 # TODO Implement this
hobby = 'singing, dancing, listening to music, playing video games'
friend1_name = 'Bagus Pribadi'
friend1_date = date(1999,2,22)
friend1_npm = 1706043941
friend1_hobby = 'coding, disturbing people, dancing, and being funny'
friend2_name = 'Anastasia Greta Elena'
friend2_date = date(1999,10,8)
friend2_npm = 1706043954
friend2_hobby = 'singing, watching korean drama, stalking wanna one'
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'hobby': hobby,
    'friend1name':friend1_name, 'friend1age':calculate_age(friend1_date.year), 'friend1npm' : friend1_npm, 
    'friend1hobby' : friend1_hobby, 'friend2name' : friend2_name, 'friend2age' : calculate_age(friend2_date.year), 
    'friend2npm' : friend2_npm, 'friend2hobby' : friend2_hobby}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
